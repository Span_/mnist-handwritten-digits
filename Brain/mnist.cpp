﻿// mnist.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


double rand01() {
	//return a random number between 0 and 1
	return static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
}


int reverseInt(int i)
{
	//used to read mnist files
	unsigned char c1, c2, c3, c4;

	c1 = i & 255;
	c2 = (i >> 8) & 255;
	c3 = (i >> 16) & 255;
	c4 = (i >> 24) & 255;

	return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
}
void read_mnist_images(const char* path, unsigned char* dataArray)
{

	std::ifstream file(path, std::ios_base::binary);
	if (file.is_open())
	{

		int dataIndex = 0;

		int magic_number = 0;
		int number_of_images = 0;
		int n_rows = 0;
		int n_cols = 0;
		file.read((char*)&magic_number, sizeof(magic_number));
		magic_number = reverseInt(magic_number);
		file.read((char*)&number_of_images, sizeof(number_of_images));
		number_of_images = reverseInt(number_of_images);
		file.read((char*)&n_rows, sizeof(n_rows));
		n_rows = reverseInt(n_rows);
		file.read((char*)&n_cols, sizeof(n_cols));
		n_cols = reverseInt(n_cols);
		for (int i = 0; i < number_of_images; ++i)
		{
			for (int r = 0; r < n_rows; ++r)
			{
				for (int c = 0; c < n_cols; ++c)
				{
					//get the number
					unsigned char temp = 0;
					file.read((char*)&temp, sizeof(temp));
					//read the number
					dataArray[dataIndex] = temp;
					dataIndex++;
				}
			}
		}
		file.close();
	}
	else {
		printf("unable to open file\n");
		system("pause");
	}
	
}
void read_mnist_labels(const char* path, unsigned char* dataArray)
{
	std::ifstream file(path, std::ios_base::binary);
	if (file.is_open())
	{


		int magic_number = 0;
		int number_of_items = 0;
		file.read((char*)&magic_number, sizeof(magic_number));
		magic_number = reverseInt(magic_number);
		file.read((char*)&number_of_items, sizeof(number_of_items));
		number_of_items = reverseInt(number_of_items);
		for (int i = 0; i < number_of_items; ++i)
		{
			//get the number
			unsigned char temp = 0;
			file.read((char*)&temp, sizeof(temp));
			//read the number
			dataArray[i] = temp;
			
		}
		file.close();
	}
	else {
		printf("unable to open file\n");
		system("pause");
	}
}


int main()
{


	const double learnRate = 1.0e-28; //magic?
	const int numInputs = 28 * 28; //the dimensions of the image 28x28
	const int numActions = 10; // the number of possible digits (0-9)
	const int numTerms = 5; //number of rows in the param array. highest degree x^n
	const int totParams = (1 + numInputs * numTerms) * numActions;// c0 + c1x1^1 + ... + cixi^1 + ... + cjxj^m

	const int testNum = 10000; //number of test images/labels
	const int trainNum = 60000; //number of training images/labels
	
	//set file locations for unzipped MNIST data
	//yann.lecun.com/exdb/mnist/
	const char* testImageFile = "../data/t10k-images-idx3-ubyte";
	const char* trainImageFile = "../data/train-images-idx3-ubyte";

	const char* testLabelsFile = "../data/t10k-labels-idx1-ubyte";
	const char* trainLabelsFile = "../data/train-labels-idx1-ubyte";

	//init arrays
	unsigned char* testImageArray = new unsigned char[testNum * numInputs];
	unsigned char* trainImageArray = new unsigned char[trainNum * numInputs];

	unsigned char* testLabelsArray = new unsigned char[testNum];
	unsigned char* trainLabelsArray = new unsigned char[trainNum];

	//get arrays
	read_mnist_images(testImageFile, testImageArray);
	read_mnist_images(trainImageFile, trainImageArray);

	read_mnist_labels(testLabelsFile, testLabelsArray);
	read_mnist_labels(trainLabelsFile, trainLabelsArray);

	//test image arrays for debugging
	//for (int m = 0; m < 10; m++) {
	//	printf(std::to_string(trainLabelsArray[m]).c_str());
	//	printf("\n");
	//	for (int i = 0; i < 28; i++) {
	//		for (int j = 0; j < 28; j++) {
	//			printf(std::to_string(trainImageArray[28*28 * m + i * 28 + j]).c_str());
	//			printf(" ");
	//		}
	//		printf("\n");
	//	}
	//	printf("-----------------------------------------\n");
	//}
	//system("pause");


	double input_array[numInputs] = { 0.0 };
	double action_array[numActions] = { 0.0 };
	double correct_action_array[numActions] = { 0.0 };
	double* param_array = new double[totParams] {0.0};// = { 0.0 };
	
	//main loop
	int p;
	double term;
	int pixelIndex = 0; //keep track of our place in the list of all pixels
	double actionLength = 0; //used to normalize action vector
	int imageIndex = 0; //which image / label we are on
	do {

		//Set values for input array (one 28x28 image)
		for (int i = 0; i < numInputs; i++) {
			input_array[i] = (double)trainImageArray[pixelIndex];
			pixelIndex++;
		}

		//var term = param_array[0];
		term = 0;
		p = 0;
		//Build the action array as a polynomial function
		for (int a = 0; a < numActions; a++) {
			action_array[a] = param_array[p];
			p++;
			for (int n = 1; n < numTerms + 1; n++) {

				term = 0; 

				for (int i = 0; i < numInputs; i++) {

					term += param_array[p] * pow(input_array[i], n);
					p++;
				}
				action_array[a] += term;

			}
		}

		//populate the correct_action_array
		for (int a = 0; a < numActions; a++) {


			if (a == trainLabelsArray[imageIndex]) {
				correct_action_array[a] = 1;
			}
			else {
				correct_action_array[a] = 0;
			}
		}

		//calculating gradient descent here
		p = 0;

		for (int a = 0; a < numActions; a++) {
			param_array[p] = param_array[p] - learnRate * 1 * (action_array[a] - correct_action_array[a]);
			p++;
			for (int n = 1; n < numTerms + 1; n++) {


				for (int i = 0; i < numInputs; i++) {

					param_array[p] = param_array[p] - learnRate * pow(input_array[i], n) * (action_array[a] - correct_action_array[a]);
					p++;

				}

			}
		}

		imageIndex++;
	} while (imageIndex < trainNum); //done training

	//check our work with the test data
	imageIndex = 0;
	pixelIndex = 0;
	double bigAction = 0.0; //highest probability action
	int correctActionIndex = 0;
	int correctActions = 0;
	do {

		//Set values for input array (one 28x28 image)
		for (int i = 0; i < numInputs; i++) {
			input_array[i] = (double)testImageArray[pixelIndex];
			pixelIndex++;
		}


		term = 0;
		p = 0;
		//Build the action array as a polynomial function
		for (int a = 0; a < numActions; a++) {
			action_array[a] = param_array[p];
			p++;
			for (int n = 1; n < numTerms + 1; n++) {

				term = 0; //param_array[n];

				for (int i = 0; i < numInputs; i++) {

					term += param_array[p] * pow(input_array[i], n);
					p++;
				}
				action_array[a] += term;
			}
		}

		//compare our action with the correct action, cacluatint % error
		//find max value
		bigAction = action_array[0];
		correctActionIndex = 0;
		for (int a = 1; a < numActions; a++) {
			if (bigAction < action_array[a]) {
				bigAction = action_array[a];
				correctActionIndex = a;
			}
			
		}
		//compare most likely action with correct action
		if (correctActionIndex == testLabelsArray[imageIndex]) correctActions++;
		
		
		
		
		//actionLength = 0;
		//for (int i = 0; i < numActions; i++) {
		//	actionLength += pow(action_array[i], 2.0);
		//}
		//actionLength = sqrt(actionLength);

		//printf(std::to_string(testLabelsArray[imageIndex]).c_str());
		//printf(": ");
		//for (int i = 0; i < numActions; i++) {
		//	printf(std::to_string(action_array[i]/actionLength).c_str());
		//	printf("  ");
		//}
		//printf("\n");

		//printf("Input:\n");
		//printf(std::to_string(testLabelsArray[imageIndex]).c_str());
		//printf("\n");
		//for (int i = 0; i < 28; i++) {
		//	for (int j = 0; j < 28; j++) {
		//		printf(std::to_string(testImageArray[28*28 * imageIndex + i * 28 + j]).c_str());
		//		printf(" ");
		//	}
		//	printf("\n");
		////}
		//printf("Output:\n");
		//for (int i = 0; i < numActions; i++) {
		//	printf(std::to_string(action_array[i]).c_str());
		//	printf(" ");
		//}
		//printf("\n-----------------------------------------\n");

		imageIndex++;
	} while (imageIndex < testNum);

	float totError = (float)correctActions / (float)testNum * 100.0f;

	printf("percent correct: ");
	printf(std::to_string(totError).c_str());
	printf("\n");




	//printf("inputs\n");
	//for (int i = 0; i < numInputs; i++) {
	//	printf("%s\n", std::to_string(input_array[i]).c_str());
	//}
	//printf("actions\n");
	//for (int i = 0; i < numActions; i++) {
	//	printf("%s\n", std::to_string(action_array[i]).c_str());
	//}
	//printf("correct actions\n");
	//for (int i = 0; i < numActions; i++) {
	//	printf("%s\n", std::to_string(correct_action_array[i]).c_str());
	//}
	system("pause");
    return 0;
}

